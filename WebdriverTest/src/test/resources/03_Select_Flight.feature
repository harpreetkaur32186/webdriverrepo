Feature: Select a flight

Scenario Outline: Select a flight
Given I am on the SelectFlight page
When I choose departure <departure> flight
	And I choose return <return> flight
Then I click Continue Button on the SelectFlight page

Examples:
    | departure					|	return 						|
    | Unified Airlines	| Unified Airlines	|