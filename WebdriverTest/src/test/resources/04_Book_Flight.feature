Feature: Book the choosen flight

Scenario Outline: Book the choosen flight
Given I am on the BookFlight page
When I enter firstname <firstnameP> and <lastnameP> of the Passenger
	And I choose meal <meal> preference
	And I choose <cardtype> CardType
	And I enter <cardnumber> in the CardNumber field
	#And I enter month <mmEXP> and year <yyyyEXP> of the Expiration
	And I enter creditcard <firstnameCC> and <lastnameCC>
Then I click SecurePurchase Button

Examples:
    | firstnameP	|	lastnameP	|	meal				|	cardtype	|	cardnumber	|	mmEXP	|	yyyyEXP	|	firstnameCC	|	lastnameCC	|	
		|	Harpreet		|	Kaur			|	Vegetarian	|	Visa			|	123456			|	05		|	2010		|	Harpreet		|	Kaur				|