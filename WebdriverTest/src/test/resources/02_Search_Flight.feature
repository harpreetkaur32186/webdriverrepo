Feature: Search a flight

  Scenario Outline: Search a flight
    Given User is loggedIn
    Given I am on the Flights page
    When I choose the <type> of the flight
	    And I enter how many <passengers> are travelling
	    And I choose from where the flight <departs>
	    And I set the fromMonth <fromMM> and fromDay <fromDD> of departure
	    And I choose where the flight <arrives>
	    And I set the toMonth <toMM> and toDay <toDD> of return
	    And I choose the service <serviceclass> preference
	    And I choose the airline <airline> preference
    Then I click Continue Button on the Flights page

  Examples: 
    | type  			|	passengers	| departs | fromMM	|	fromDD 	| arrives | toMM	|	toDD	|	serviceclass		| airline						|
    | oneway		 	| 1						| London	|	5				|	12			|	Paris		|	 5		|	 15		|	Business				|	Unified Airlines	|