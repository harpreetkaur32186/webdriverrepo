Feature: Login to the MercuryTours Website
	
Scenario Outline: Login to the MercuryTours
Given I am on the Home page
When I enter <username> in the UserName Field
	And I enter <password> in the Password Field
	And I click Submit Button
Then User is successfully loggedIn

Examples: 
    | username	|	password	|
    | hkuser 		| hkpass		|