package common;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class PageCommon {
	private static PageCommon instance;	
	private WebDriver driver;

	private PageCommon(){
		this.driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}
	
	public static PageCommon getInstance(){
		if (instance == null){
			instance = new PageCommon();
		}
		return instance;
	}

	public WebDriver getDriver() {
		return driver;
	}
	
	void setDriver(WebDriver driver){
		this.driver = driver;
	}
}	
