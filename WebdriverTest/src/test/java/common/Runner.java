package common;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions (
		features = "src/test/resources", 
		glue = {"pagefactory"},
		plugin = {"com.cucumber.listener.ExtentCucumberFormatter:output/report.html"}
		)
public class Runner extends AbstractTestNGCucumberTests {

}
