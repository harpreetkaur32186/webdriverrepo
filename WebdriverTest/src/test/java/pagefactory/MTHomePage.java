package pagefactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import common.PageCommon;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class MTHomePage {
	WebDriver driver;
	
	public MTHomePage(){
		driver = PageCommon.getInstance().getDriver();
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(name="userName")
	WebElement homePageUserName;
	
	@FindBy(name="password")
	WebElement homePagePassword;
	
	@FindBy(name="login")
	WebElement submitButton;
	
	@Given("^I am on the Home page$")
	public void userOnHomePage() {
		driver.navigate().to("http://newtours.demoaut.com/mercurywelcome.php");
	}
	
	@When("^I enter ([^\"]*) in the UserName Field$")
	public void setUserName(String username)
	{
		homePageUserName.sendKeys(username);
	}
	
	@When("^I enter ([^\"]*) in the Password Field$")
	public void setPassword(String password)
	{
		homePagePassword.sendKeys(password);
	}
	
	@When("^I click Submit Button$")
	public void clickLogin()
	{
		submitButton.click();
	}
	
	@Then("^User is successfully loggedIn$")
	public void successfulLogIn() {
		System.out.println("Successfully logged in.");
	}
	
	@Given("^User is loggedIn$")
	public void givenUserIsLoggedIn() {
		System.out.println("Welcome User, you can search the flights now.");
	}
	
	public void loginToMT(String username, String password)
	{
		this.setUserName(username);
		this.setPassword(password);
		this.clickLogin();
	}
}
