package pagefactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import common.PageCommon;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class MTBookFlightPage {
	WebDriver driver;
	
	public MTBookFlightPage(){
		driver = PageCommon.getInstance().getDriver();
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(name="passFirst0")
	WebElement firstnamePass;
	
	@FindBy(name="passLast0")
	WebElement lastnamePass;
	
	@FindBy(name="pass.0.meal")
	WebElement mealDropDwn;
	
	@FindBy(name="creditCard")
	WebElement ccTypeDropDwn;
	
	@FindBy(name="creditnumber")
	WebElement ccNumber;
	
	@FindBy(name="cc_exp_dt_mn")
	WebElement ccMonthExp;
	
	@FindBy(name="cc_exp_dt_yr")
	WebElement ccYearExp;
	
	@FindBy(name="cc_frst_name")
	WebElement firstnameCC;
	
	@FindBy(name="cc_last_name")
	WebElement lastnameCC;
	
	@FindBy(name="buyFlights")
	WebElement purchaseButton;
	
	@Given("^I am on the BookFlight page$")
	public void userOnBookFlightPage() {
		System.out.println("I am on the Book flight page");
	}

	@When("^I enter firstname ([^\"]*) and ([^\"]*) of the Passenger$")
	public void setPassengerName(String firstname, String lastname) {
		firstnamePass.sendKeys(firstname);
		lastnamePass.sendKeys(lastname);
	}

	@When("^I choose meal ([^\"]*) preference$")
	public void setMealPreference(String meal) {
		Select oSelect = new Select(mealDropDwn);
		oSelect.selectByVisibleText(meal);
	}

	@When("^I choose ([^\"]*) CardType$")
	public void setCardType(String cardType) {
		Select oSelect = new Select(ccTypeDropDwn);
		oSelect.selectByVisibleText(cardType);
	}

	@When("^I enter ([^\"]*) in the CardNumber field$")
	public void setCardNumber(String cardNumber) {
		ccNumber.sendKeys(cardNumber);
	}

	@When("^I enter month ([^\"]*) and year ([^\"]*) of the Expiration$")
	public void setCCExpiration(String ccMonth, String ccYear) {
		Select oSelectM = new Select(ccMonthExp);
		oSelectM.selectByValue(ccMonth);
		Select oSelectY = new Select(ccYearExp);
		oSelectY.selectByValue(ccYear);
	}

	@When("^I enter creditcard ([^\"]*) and ([^\"]*)$")
	public void setCreditCardName(String firstname, String lastname) {
		firstnameCC.sendKeys(firstname);
		lastnameCC.sendKeys(lastname);
	}

	@Then("^I click SecurePurchase Button$")
	public void clickSecurePurchaseButton() {
		purchaseButton.click();
	}
}
