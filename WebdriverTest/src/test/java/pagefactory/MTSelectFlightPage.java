package pagefactory;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import common.PageCommon;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class MTSelectFlightPage {
	WebDriver driver;
	
	public MTSelectFlightPage(){
		driver = PageCommon.getInstance().getDriver();
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(name="outFlight")
	List<WebElement> outFlightRadioBtn;
	
	@FindBy(name="inFlight")
	List<WebElement> inFlightRadioBtn;
	
	@FindBy(name="reserveFlights")
	WebElement continueButton;
	
	@Given("^I am on the SelectFlight page$")
	public void userOnSelectFlightPage() {
		System.out.println("I am on the Select flight page");
	}

	@When("^I choose departure ([^\"]*) flight$")
	public void setDepartureFlight(String departureFlight) {
		List<WebElement> oRadioBtn = outFlightRadioBtn;
		int iSize = oRadioBtn.size();
		for(int i=0; i<iSize; i++)
		{
			String sValue = oRadioBtn.get(i).getAttribute(("value"));
			if(sValue.contains(departureFlight)){
				oRadioBtn.get(i).click();
				break;
			}
		}
	}

	@When("^I choose return ([^\"]*) flight$")
	public void setReturnFlight(String returnFlight) {
		List<WebElement> oRadioBtn = inFlightRadioBtn;
		int iSize = oRadioBtn.size();
		for(int i=0; i<iSize; i++)
		{
			String sValue = oRadioBtn.get(i).getAttribute(("value"));
			if(sValue.contains(returnFlight)){
				oRadioBtn.get(i).click();
				break;
			}
		}
	}
	
	@Then("^I click Continue Button on the SelectFlight page$")
	public void clickContinueButton() {
		continueButton.click();
	}
}
