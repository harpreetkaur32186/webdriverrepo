package pagefactory;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import common.PageCommon;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class MTSearchFlightPage {
	WebDriver driver;
	
	public MTSearchFlightPage(){
		driver = PageCommon.getInstance().getDriver();
		PageFactory.initElements(driver, this);
	}

	@FindBy(name="tripType")
	List<WebElement> tripTypeRadioBtn;
	
	@FindBy(name="passCount")
	WebElement passengersDropDwn;
	
	@FindBy(name="fromPort")
	WebElement departureDropDwn;
	
	@FindBy(name="fromMonth")
	WebElement fromMonthDropDwn;
	
	@FindBy(name="fromDay")
	WebElement fromDayDropDwn;
	
	@FindBy(name="toPort")
	WebElement arrivalDropDwn;
	
	@FindBy(name="toMonth")
	WebElement toMonthDropDwn;
	
	@FindBy(name="toDay")
	WebElement toDayDropDwn;
	
	@FindBy(name="servClass")
	List<WebElement> serviceClassPreferenceRadioBtn;
	
	@FindBy(name="airline")
	WebElement airlinePreferenceDropDwn;
	
	@FindBy(name="findFlights")
	WebElement continueButton;
	
	@Given("^I am on the Flights page$")
	public void userOnFlightsPage() {
		System.out.println("I am on the Search flights page");
	}
	
	@When("^I choose the ([^\"]*) of the flight$")
	public void setFlightType(String flightType) {
		List<WebElement> oRadioBtn = tripTypeRadioBtn;
		int iSize = oRadioBtn.size();
		for(int i=0; i<iSize; i++)
		{
			String sValue = oRadioBtn.get(i).getAttribute("value");
			if(sValue.contains(flightType)){
				oRadioBtn.get(i).click();
				break;
			}
		}
	}

	@When("^I enter how many ([^\"]*) are travelling$")
	public void setPassengersCount(String passCount) {
		Select oSelect = new Select(passengersDropDwn);
		oSelect.selectByValue(passCount);
	}

	@When("^I choose from where the flight ([^\"]*)$")
	public void setPlaceOfFlightDeparture(String departurePlace) {
		Select oSelect = new Select(departureDropDwn);
		oSelect.selectByValue(departurePlace);
	}

	@When("I set the fromMonth ([^\"]*) and fromDay ([^\"]*) of departure$")
	public void setDepartureDate(String fromMonth, String fromDay) {
		Select oSelectM = new Select(fromMonthDropDwn);
		oSelectM.selectByValue(fromMonth);
		Select oSelectD = new Select(fromDayDropDwn);
		oSelectD.selectByValue(fromDay);
	}

	@When("^I choose where the flight ([^\"]*)$")
	public void setPlaceOfFlightArrival(String arrivalPlace) {
		Select oSelect = new Select(arrivalDropDwn);
		oSelect.selectByValue(arrivalPlace);
	}

	@When("^I set the toMonth ([^\"]*) and toDay ([^\"]*) of return$")
	public void setReturnDate(String toMonth, String toDay) {
		Select oSelectM = new Select(toMonthDropDwn);
		oSelectM.selectByValue(toMonth);
		Select oSelectD = new Select(toDayDropDwn);
		oSelectD.selectByValue(toDay);
	}

	@When("^I choose the service ([^\"]*) preference$")
	public void setServiceClassPreference(String serviceClass) {
		List<WebElement> oRadioBtn = serviceClassPreferenceRadioBtn;
		int iSize = oRadioBtn.size();
		for(int i=0; i<iSize; i++)
		{
			String sValue = oRadioBtn.get(i).getAttribute("value");
			if(sValue.contains(serviceClass)){
				oRadioBtn.get(i).click();
				break;
			}
		}
	}

	@When("^I choose the airline ([^\"]*) preference$")
	public void setAirlinePreference(String airline) {
		Select oSelect = new Select(airlinePreferenceDropDwn);
		oSelect.selectByVisibleText(airline);
	}
	
	@Then("^I click Continue Button on the Flights page$")
	public void clickContinueButton() {
		continueButton.click();
	}
}
